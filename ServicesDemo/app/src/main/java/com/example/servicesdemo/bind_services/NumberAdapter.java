package com.example.servicesdemo.bind_services;


import android.graphics.Color;
import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.example.servicesdemo.R;

public class NumberAdapter extends RecyclerView.Adapter<NumberAdapter.ViewHolder> {
    private int selectedPosition;
    private int generatedPosition = -1;

    @NonNull
    @Override
    public NumberAdapter.ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int i) {
        View view = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.luckynumber, parent, false);
        return new NumberAdapter.ViewHolder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull NumberAdapter.ViewHolder viewHolder, final int i) {
        viewHolder.tvNumber.setText(String.valueOf(i));

        if (selectedPosition == i) {
            viewHolder.tvNumber.setTextColor(Color.RED);
        } else if (generatedPosition == i) {
            viewHolder.tvNumber.setTextColor(Color.GREEN);
        } else {
            viewHolder.tvNumber.setTextColor(Color.BLACK);
        }

        viewHolder.tvNumber.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                generatedPosition = -1;
                selectedPosition = i;
                notifyDataSetChanged();
            }
        });
    }

    @Override
    public int getItemCount() {
        return 9;
    }

    public class ViewHolder extends RecyclerView.ViewHolder {
        public TextView tvNumber;

        public ViewHolder(@NonNull View itemView) {
            super(itemView);
            tvNumber = itemView.findViewById(R.id.tv_num);
        }
    }


    public void setGeneratedPosition(int generatedPosition) {
        this.generatedPosition = generatedPosition;
        notifyItemChanged(generatedPosition);


    }

    public int getSelectedPosition() {
        return selectedPosition;
    }
}
