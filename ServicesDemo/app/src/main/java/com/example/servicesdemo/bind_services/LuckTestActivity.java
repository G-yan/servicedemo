package com.example.servicesdemo.bind_services;

import android.content.ComponentName;
import android.content.Context;
import android.content.Intent;
import android.content.ServiceConnection;
import android.graphics.Color;
import android.os.IBinder;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.GridLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.widget.TextView;

import com.example.servicesdemo.R;

public class LuckTestActivity extends AppCompatActivity {

    private RecyclerView rvLuckyDraw;
    private TextView testResult;
    MyRandomGeneratorService randomGeneratorService;
    boolean isBound = false;
    private NumberAdapter numberAdapter;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_luck_test);

        rvLuckyDraw = findViewById(R.id.rv_lucky_draw);
        testResult = findViewById(R.id.tv_text_result);

        numberAdapter = new NumberAdapter();
        rvLuckyDraw.setLayoutManager(new GridLayoutManager(LuckTestActivity.this, 3));
        rvLuckyDraw.setAdapter(numberAdapter);

        findViewById(R.id.btn_check).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                checkLuck();
            }
        });


        Intent intent = new Intent(this, MyRandomGeneratorService.class);
        bindService(intent, serviceConnection, Context.BIND_AUTO_CREATE);
    }

    private void checkLuck() {
        if (numberAdapter == null)
            return;numberAdapter.setGeneratedPosition(-1);
        numberAdapter.setGeneratedPosition(-1);
        int random = randomGeneratorService.getRandom();
        numberAdapter.setGeneratedPosition(random);

        if (random == numberAdapter.getSelectedPosition()) {
            testResult.setText("Today is your lucky day");
            testResult.setTextColor(Color.GREEN);
        } else {
            testResult.setText("Be Careful Today");
            testResult.setTextColor(Color.RED);
        }
    }

    ServiceConnection serviceConnection = new ServiceConnection() {
        @Override
        public void onServiceConnected(ComponentName name, IBinder service) {
            MyRandomGeneratorService.LocalBinder localBinder = (MyRandomGeneratorService.LocalBinder) service;
            randomGeneratorService = localBinder.getRandomgenerator();
            isBound = true;
        }

        @Override
        public void onServiceDisconnected(ComponentName name) {
            isBound = false;
        }
    };
}
