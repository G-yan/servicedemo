package com.example.servicesdemo.bind_services;

import android.app.Service;
import android.content.Intent;
import android.os.Binder;
import android.os.IBinder;

import java.util.Random;

public class MyRandomGeneratorService extends Service {
    private final IBinder binder = new LocalBinder();
    private final Random randomGenerator = new Random();

    public class LocalBinder extends Binder {

        MyRandomGeneratorService getRandomgenerator() {
            return MyRandomGeneratorService.this;
        }
    }

    @Override
    public IBinder onBind(Intent intent) {
        return binder;
    }


    int getRandom() {
        return randomGenerator.nextInt(9);
    }
}
